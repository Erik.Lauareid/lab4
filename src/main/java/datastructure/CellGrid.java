package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int cols;
    
    private CellState [][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        this.grid = new CellState[rows][columns];
        
        
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.cols; col++) {
                this.grid[row][col] = initialState;
            }
           
        }

		
	}

    @Override
    public int numRows() {
        
        return this.rows;
    }

    @Override
    public int numColumns() {
        
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        
        if (row<0 || row>numRows()) {
            throw new IndexOutOfBoundsException();
        }
        if (column<0 || column>numColumns()) {
            throw new IndexOutOfBoundsException();
        }
        this.grid[row][column] = element;
        
        // TODO Auto-generated method stub
        
    }

    @Override
    public CellState get(int row, int column) {
        if (row<0 || row>numRows()) {
            throw new IndexOutOfBoundsException();
        }
        if (column<0 || column>numColumns()) {
            throw new IndexOutOfBoundsException();
        }
        
        
        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid theCopy = new CellGrid(this.rows, this.cols, null);

        
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.cols; col++) {
                theCopy.set(row, col, this.get(row, col));
                
                
            }
            
        }
        
        return theCopy;
    }
    
}
